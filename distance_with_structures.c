//WAP to find the distance between two points using structures and 4 functions.

#include<stdio.h>
#include<math.h>
struct point
{
float a,b;
};
typedef struct point Point;
Point input()
{
Point p;
printf("enter coordinates of point1\n");
scanf("%f",&p.a);
printf("enter coordinates of point2\n");
scanf("%f",&p.b);
return p;
}
float compute(Point p1,Point p2)
{
float distance;
distance=sqrt(pow((p2.a-p1.a),2)+pow((p2.b-p1.b),2));
return distance;
}
void output(Point p1,Point p2,float d)
{
printf("the distance between (%f,%f)and(%f,%f) is %f",p1.a,p1.b,p2.a,p2.b,d);
}

int main()
{
float d;
Point p1,p2;
p1=input();
p2=input();
d=compute(p1,p2);
output(p1,p2,d);
return 0;
}
